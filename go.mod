module opensauce.de/bdd

go 1.17

require (
	github.com/cucumber/common/gherkin/go/v22 v22.0.0
	github.com/cucumber/common/messages/go/v17 v17.1.1
	github.com/go-bdd/assert v0.0.0-20200713105154-236f01430281
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776
)

replace github.com/go-bdd/gobdd => ../../Go/src/github.com/go-bdd/gobdd

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gofrs/uuid v4.0.0+incompatible // indirect
)
