package parser

import (
	"io" // only for io.Reader interface definition?!

	// TODO: 2021-10-28 remove dependency on cucumber
	//					define our own structure and provide a mapper to
	//					translate from the AST provided by cucumber to our
	//					internal structure
	msgs "github.com/cucumber/common/messages/go/v17"
)

// GherkinParser is a consumption interface (dependency inversion) used to
// convert a gherkin file (text) into an Abstract Syntax Tree
type GherkinParser interface {
	ParseGherkinDocument(io.Reader, func() string) (*msgs.GherkinDocument, error)
}

// GherkinPickler is a consumption interface (dependency inversion) used to
// convert a gherkin AST into a stream of 'pickles' for step executution
type GherkinPickler interface {
	Pickles(msgs.GherkinDocument, string, func() string) []*msgs.Pickle
}
