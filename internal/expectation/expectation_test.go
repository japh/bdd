package expectation_test

import (
	"fmt"
	"testing"

	is "opensauce.de/bdd/internal/expectation"
)

type testReporter struct {
	gotError bool
	message  string
}

func (t *testReporter) Error(message ...interface{}) {
	t.gotError = true
	t.message = message[0].(string)
}

func (t *testReporter) Log(message ...interface{}) {
}

func (t *testReporter) Logf(format string, message ...interface{}) {
}

func (t *testReporter) clear() {
	t.gotError = false
	t.message = ""
}

func TestNilExpectation(t *testing.T) {
	tests := []struct {
		value      interface{}
		shouldPass bool
		goal       string
		name       string
	}{
		{value: nil, shouldPass: true, goal: "explicit Nil() check"},
		{value: nil, shouldPass: true, name: "explicit Nil() check without goal"},
		{value: 1, shouldPass: false, goal: "explicit anti-Nil() check with int"},
		{value: 1, shouldPass: false, name: "explicit anti-Nil() check with int without goal"},
		{value: "hello", shouldPass: false, goal: "explicit anti-Nil() check with string"},
		{value: "hello", shouldPass: false, name: "explicit anti-Nil() check with string without goal"},
	}

	for _, tt := range tests {
		name := tt.name
		if name == "" {
			name = tt.goal
		}
		name = fmt.Sprintf("NotNil(%v) %s", tt.value, name)

		t.Run(name, func(t *testing.T) {
			r := &testReporter{}
			didPass := is.Nil(r, tt.value, tt.goal)
			elaborate(t, tt.shouldPass, didPass, r, name)
		})
	}
}

func TestNotNilExpectation(t *testing.T) {
	tests := []struct {
		value      interface{}
		shouldPass bool
		goal       string
		name       string
	}{
		{value: nil, shouldPass: false, goal: "explicit NotNil() check"},
		{value: nil, shouldPass: false, name: "explicit NotNil() check without goal"},
		{value: 1, shouldPass: true, goal: "explicit anti-NotNil() check with int"},
		{value: 1, shouldPass: true, name: "explicit anti-NotNil() check with int without goal"},
		{value: "hello", shouldPass: true, goal: "explicit anti-NotNil() check with string"},
		{value: "hello", shouldPass: true, name: "explicit anti-NotNil() check with string without goal"},
	}

	for _, tt := range tests {
		name := tt.name
		if name == "" {
			name = tt.goal
		}
		name = fmt.Sprintf("NotNil(%v) %s", tt.value, name)

		t.Run(name, func(t *testing.T) {
			r := &testReporter{}
			didPass := is.NotNil(r, tt.value, tt.goal)
			elaborate(t, tt.shouldPass, didPass, r, name)
		})
	}
}

func TestEqualExpectation(t *testing.T) {

	type testObj struct {
		a string
		b bool
	}

	a := testObj{"hello", true}
	b := testObj{"hello", true}
	c := testObj{"bye", true}
	d := testObj{"hello", false}

	tests := []struct {
		want       interface{}
		got        interface{}
		shouldPass bool
		goal       string
		name       string
	}{
		{want: nil, got: nil, shouldPass: true, goal: "nil == nil"},
		{want: nil, got: 1, shouldPass: false, goal: "nil != 1"},
		{want: nil, got: "hello", shouldPass: false, goal: "nil != hello"},
		{want: 1, got: 1, shouldPass: true, goal: "1 == 1"},
		{want: 1, got: 2, shouldPass: false, goal: "1 == 2"},
		{want: "hello", got: "hello", shouldPass: true, goal: "hello == hello"},
		{want: "hello", got: "bye", shouldPass: false, goal: "hello != bye"},
		{want: a, got: a, shouldPass: true, goal: "a == a"},
		{want: a, got: b, shouldPass: true, goal: "a == b"},
		{want: a, got: c, shouldPass: false, goal: "a != c"},
		{want: a, got: d, shouldPass: false, goal: "a != d"},
		{want: a, got: &a, shouldPass: false, goal: "a != &a"},
		{want: &a, got: &a, shouldPass: true, goal: "&a == &a"},
		{want: &a, got: &b, shouldPass: true, goal: "&a == &b"},
		{want: &a, got: &c, shouldPass: false, goal: "&a != &c"},
		{want: &a, got: &d, shouldPass: false, goal: "&a != &d"},
	}

	for _, tt := range tests {
		name := tt.name
		if name == "" {
			name = tt.goal
		}
		name = fmt.Sprintf("Equal(%v,%v) %s", tt.want, tt.got, name)

		t.Run(name, func(t *testing.T) {
			r := &testReporter{}
			didPass := is.Equal(r, tt.want, tt.got, tt.goal)
			elaborate(t, tt.shouldPass, didPass, r, name)
		})
	}
}

func elaborate(t *testing.T, wantPass, didPass bool, r *testReporter, name string) {
	if wantPass {
		if !didPass {
			t.Errorf("%s should have passed, but failed unexpectedly", name)
		}
		if r.gotError {
			t.Errorf("%s generated an unexpected error%s", name, r.message)
		}
	} else {
		if didPass {
			t.Errorf("%s should have failed, but passed unexpectedly", name)
		}
		if !r.gotError {
			t.Errorf("%s failed to generate an error", name)

		}
	}
}
