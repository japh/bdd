// Package expectation is a wrapper for testing.T providing READABLE test messages:
//      passing tests are logged
//      failing tests show wanted and received values under each other, making
//      differences easier to identify
//
// Inspriation:
//		I find the default formatting of t.Errorf() and %v impossible to read.
//		https://www.arp242.net/go-testing-style.html give's a few 'style
//		hints', and this just takes it one step further (while only providing a
//		minimal set of comparitors)
//
// BUG: 2021-10-28 logged line numbers come from passed() / failed() (below)
//                 and not the caller of the caller to Nil(), Equal() etc.
//                 (e.g. the place where Equal() was called)
package expectation

import (
	"bytes"
	"fmt"
	"reflect"
	"strings"
)

// ErrorReporter is a minimal testing.T interface (derived from go's standard
// testing package).
// This is basically 'dependency inversion', so that we can define out own
// (mock) testing package(s) as needed.
type ErrorReporter interface {
	Error(...interface{})
	Log(...interface{})
	Logf(string, ...interface{})
}

// Nil compares a value against nil
// Special care is taken to compare interfaces, functions, pointers to structs etc. properly
// If the value is nil, the goal is logged via t.Log() and true is returned
// Otherwise, t.Error() is used to report the error and false is returned
func Nil(t ErrorReporter, got interface{}, goal ...string) bool {
	if !isNil(got) {
		return failed(t, nil, got, goal, "nil")
	}
	return passed(t, goal, "nil")
}

// NotNil compares a value against nil
// Special care is taken to compare interfaces, functions, pointers to structs etc. properly
// If the value is not nil, the goal is logged via t.Log() and true is returned
// Otherwise, t.Error() is used to report the error and false is returned
func NotNil(t ErrorReporter, got interface{}, goal ...string) bool {
	if isNil(got) {
		return failed(t, fmt.Sprintf("not %v", nil), got, goal, "not nil")
	}
	return passed(t, goal, "not nil")
}

// Equal compares a value against an expected value
// Special care is taken to compare interfaces, functions, pointers to structs etc. properly
// If the values are the same, the goal is logged via t.Log() and true is returned
// Otherwise, t.Error() is used to report the error and false is returned
func Equal(t ErrorReporter, want, got interface{}, goal ...string) bool {
	if !objectsAreEqual(want, got) {
		return failed(t, want, got, goal, "equal")
	}
	return passed(t, goal, "equal")
}

// NotEqual compares a value against a value which is not expected
// Special care is taken to compare interfaces, functions, pointers to structs etc. properly
// If the values are not the same, the goal is logged via t.Log() and true is returned
// Otherwise, t.Error() is used to report the error and false is returned
func NotEqual(t ErrorReporter, want, got interface{}, goal ...string) bool {
	if objectsAreEqual(want, got) {
		return failed(t, fmt.Sprintf("not %v", want), got, goal, "not equal")
	}
	return passed(t, goal, "not equal")
}

func objectsAreEqual(want, got interface{}) bool {
	if isNil(want) || isNil(got) {
		return isNil(want) == isNil(got)
	}

	if isFunction(want) || isFunction(got) {
		panic("Not possible to compare functions")
	}

	w, ok := want.([]byte)
	if !ok {
		return reflect.DeepEqual(want, got)
	}

	g, ok := got.([]byte)
	if !ok {
		return false
	}

	if isNil(g) || isNil(w) {
		return isNil(g) && isNil(w)
	}

	return bytes.Equal(g, w)
}

func isNil(i interface{}) bool {
	if i == nil {
		return true
	}
	switch reflect.TypeOf(i).Kind() {
	case reflect.Ptr, reflect.Map, reflect.Array, reflect.Chan, reflect.Slice, reflect.Func:
		return reflect.ValueOf(i).IsNil()
	}
	return false
}

func isFunction(x interface{}) bool {
	return reflect.TypeOf(x).Kind() == reflect.Func
}

func passed(t ErrorReporter, goal []string, should string) bool {
	b := &strings.Builder{}
	if len(goal) == 0 || (len(goal) == 1 && goal[0] == "") {
		fmt.Fprintf(b, "pass: got expected %s", should)
	} else {
		fmt.Fprintf(b, "pass: %s", strings.Join(goal, " "))
	}
	t.Log(b.String())
	return true
}

func failed(t ErrorReporter, want, got interface{}, goal []string, should string) bool {
	b := &strings.Builder{}
	if len(goal) == 0 || (len(goal) == 1 && goal[0] == "") {
		fmt.Fprintf(b, "\nfail: should be %s\n", should)
	} else {
		fmt.Fprintf(b, "\nfail: %s\n", strings.Join(goal, " "))
	}
	if !isNil(want) {
		fmt.Fprintf(b, "want: %v\n", want)
	}
	fmt.Fprintf(b, "got:  %v", got)
	t.Error(b.String())
	return false
}
