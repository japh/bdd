//
// Package bdd is yet another BDD / gherkin tool
//
package bdd

import (
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"strings"

	gherkin "github.com/cucumber/common/gherkin/go/v22"
	msgs "github.com/cucumber/common/messages/go/v17"
)

// Tester is a generic placeholder for testing.T or similar
type Tester interface{}

// Suite tracks a set of gherkin documents its step definitions and the
// results of any tests
type Suite struct {
	tester           Tester
	options          suiteOptions
	gherkinDocuments map[string]*gherkinDocument // indexed by filename
	shortNames       map[string][]string         // map basename of feature file to full path, maybe ambiguous
	errors           []error
}

type gherkinDocument struct {
	uri   string                // file name found before parsing
	state string                // unknown, missing, unreadable, unparsable, parsed, failed, passed
	ast   *msgs.GherkinDocument // the AST returned from gherkin.ParseGherkinDocument()
}

// SuiteOptions holds all the information about how the suite or features/steps should be configured
type suiteOptions struct {
	searchPaths []string
}

// NewSuite creates a new BDD test suite to be used with one or more gherkin
// documents
func NewSuite(t Tester, options ...SuiteOption) (s *Suite) {
	s = &Suite{
		tester:  t,
		options: newSuiteOptions(),
		errors:  make([]error, 0),
	}

	for _, o := range options {
		o(s)
	}

	return
}

// Run runs the tests specified in the gherkin document(s) using the step
// definitions that have been defined in the test suite via s.Given(), s.When()
// or s.Then()
func (s *Suite) Run() {
	// call gherkin.Pickles() for each gherkin document
}

// Options should return a hash of options - only used for testing?
func (s *Suite) Options() []string {
	o := make([]string, len(s.options.searchPaths))
	i := 0
	for _, p := range s.options.searchPaths {
		o[i] = fmt.Sprintf("Search Path: %s", p)
		i++
	}
	return o
}

// SuiteOption is the base type used for changing a bdd.Suite's configuration
// options.
//
// Usage:
//	s := bdd.NewSuite(testing.T, bdd.With...(...))
// or:
//	s := bdd.NewSuite(t)
//	bdd.With...(...)(s)
type SuiteOption func(s *Suite)

func newSuiteOptions() suiteOptions {
	return suiteOptions{
		searchPaths: []string{"features/"},
	}
}

// WithFeaturesPaths sets the list of paths used by the bdd.Suite to find its
// gherkin files.
// The list of paths may be any combination of directories or file names.
func WithFeaturesPaths(paths ...string) SuiteOption {
	return func(s *Suite) {
		s.options.searchPaths = paths
		s.gherkinDocuments = nil
	}
}

// Errors returns a list of errors encountered so far
func (s *Suite) Errors() []error {
	return s.errors
}

// gherkinDocumentNames returns the names of all found Gherkin documents
func (s *Suite) gherkinDocumentNames() []string {
	if s.gherkinDocuments == nil {
		s.findGherkinDocuments()
	}

	names := make([]string, len(s.gherkinDocuments))
	i := 0
	for n := range s.gherkinDocuments {
		names[i] = n
		i++
	}

	return names
}

func (s *Suite) gherkinDocumentPath(shortName string) (string, error) {
	if s.gherkinDocuments == nil {
		s.findGherkinDocuments()
	}

	uris, ok := s.shortNames[shortName]
	if ok != true {
		return "", fmt.Errorf("Found no gherkin files matching '%s'", shortName)
	}

	if len(uris) != 1 {
		return "", fmt.Errorf("Ambiguous short feature name '%s' matches %d files\n%#v", shortName, len(uris), uris)
	}

	return uris[0], nil
}

func (s *Suite) gherkinAST(path string) (*msgs.GherkinDocument, error) {
	g, ok := s.gherkinDocuments[path]
	if ok != true {
		return nil, fmt.Errorf("'%s' was not found in this BDD suite's search path", path)
	}

	ast, err := g.gherkinAST()
	if err != nil {
		return nil, err
	}

	return ast, nil
}

// GherkinPickles parses a gherkin document and returns a series of scenario
// steps to be executed.
// Pickles are intended for direct use for testing!
// backgrounds and Scenario Outlines are expanded so that the steps are
// repeated for the entire scenario, possibly with different parameters.
func (s *Suite) GherkinPickles(path string) ([]*msgs.Pickle, error) {
	g, ok := s.gherkinDocuments[path]
	if ok != true {
		return nil, fmt.Errorf("'%s' was not found in this BDD suite's search path", path)
	}

	_, err := g.gherkinAST()
	if err != nil {
		return nil, err
	}

	return g.featurePickles()
}

func (s *Suite) findGherkinDocuments() {
	root, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	s.gherkinDocuments = make(map[string]*gherkinDocument)
	s.shortNames = make(map[string][]string)

	for _, p := range s.options.searchPaths {
		if !filepath.IsAbs(p) {
			p = filepath.Join(root, p)
		}
		dirFileCount := 0
		err := filepath.WalkDir(p, s.gherkinDocumentFinder(&dirFileCount))
		if err != nil {
			s.errors = append(s.errors, err)
		}
		if dirFileCount == 0 {
			s.errors = append(s.errors, fmt.Errorf("Found no gherkin documents in '%s'", p))
		}
	}
}

func (s *Suite) gherkinDocumentFinder(foundCount *int) fs.WalkDirFunc {
	return func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}

		bn := filepath.Base(path)
		sn := strings.TrimSuffix(bn, `.feature`)
		if matched, _ := filepath.Match(`*.feature`, bn); matched == true {
			*foundCount++
			s.gherkinDocuments[path] = &gherkinDocument{uri: path, state: `unread`}
			if _, ok := s.shortNames[sn]; ok != true {
				s.shortNames[sn] = make([]string, 0)
			}
			s.shortNames[sn] = append(s.shortNames[sn], path)
		}

		return nil
	}
}

func (g *gherkinDocument) gherkinAST() (*msgs.GherkinDocument, error) {

	r, err := os.Open(g.uri)
	if err != nil {
		g.state = `unreadable`
		return nil, err
	}

	g.ast, err = gherkin.ParseGherkinDocument(r, (&msgs.Incrementing{}).NewId)
	if err != nil {
		g.state = `unparsable`
		return nil, err
	}

	return g.ast, nil
}

func (g *gherkinDocument) featurePickles() ([]*msgs.Pickle, error) {
	if g.ast == nil {
		return nil, fmt.Errorf("no pickles for '%s'", g.uri)
	}
	return gherkin.Pickles(*g.ast, g.uri, (&msgs.Incrementing{}).NewId), nil
}
