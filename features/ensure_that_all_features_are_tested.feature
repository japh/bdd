Business Need: ensure that all features are tested

  As a developer
  I want to ensure that no features are untested before committing any changes
  So that the statement "no errors" actually means "all features passed"

  Rule: every feature must be tested when running "make test" without options

    # note: a -tags filter will obviously only run a subset of all feature tests
    # however, it should still be possible to determine that all step
    # definitions are present and at least testable

    Scenario: warn when running bdd tests on a project without any gherkin documents
      Given a project without any gherkin documents
       When the project is BDD tested
       Then a warning should be produced indicating that no gherkin documents were found

    Scenario: no warning if at least 1 gherkin document can be found
      Given a project with a gherkin document "features/do_nothing.feature"
       When the project is BDD tested
       Then no warning about missing gherkin documents should be produced

