Ability: run a test for a single step

  # what
  One of the central features of Behaviour Driven Development (BDD) is to
  provide a verifiable connection between human expectations of a system and
  their technical implementations.

  # why

  # how
  Gherkin is intended to help visualise the features of a system in the natural
  language of all of its stakeholders (product owners, users, developers, etc).
  Features are specified individually, each being subdivided into separate
  rules, scenarios and steps.

  Glossary of terms:

    Step:
      The smallest unit of description or testing, at a level which directly
      affects an end-user of the system.

    Step Description
      The human language description of a single step.

    Step Function
      The technical implementation of a single step.

    Step Definition
      Not used here!

      The Cucumber developers use "Step Definition" to describe what we call a
      "Step Function".

      We avoid the term here because the word "definition" (in english) implies
      more a "definitive description" of something, rather than its "technical
      implementation"

      See: https://cucumber.io/docs/cucumber/step-definitions/

  Rule: when a step description is tested, exactly one of the valid step test result states must be returned

    # Note: this is not valid gherkin (according to cucumber)
    #       however, it SHOULD be valid, since any lines which do not match
    #       known keywords should be considered as 'descriptive text'
    # TODO: 2021-11-04 turn this table into a suitable scenario and/or steps
    Possible Step Test Result States:
      | context  | state       | description                                                                                          |
      | step     | failed      | the step description and function both exist, but its execution failed                               |
      | step     | ok          | the step description and function both exist and its execution completed successfully                |
      | step     | pending     | a step description and function both exist, but the function has not been completely implemented yet |
      | step     | missing     | the step function corresponding to an existing step description could not be found                   |
      | step     | unspecified | the step description corresponding to an existing step function could not be found                   |
      | scenario | skipped     | the step function exists, but was not run because a previous step did not complete successfully      |
      | scenario | filtered    | the step function exists, but was not run because it was explicitly ignored (e.g. via tag filter)    |

    Scenario: missing step functions should fail

      Given a step description "When I eat a gherkin"
        But no step function for "When I eat a gherkin" exists
       When the step is tested
       Then the step should fail
        And the step's test status should be "missing"

    Scenario: pending step functions should fail

      Given a step description "When I eat a gherkin"
        And a step function for "When I eat a gherkin" exists
       When the step is tested
        And the step function does not report success or failure
       Then the step should fail
        And the step's test status should be "pending"

    Scenario: failing step functions

      Given a step description "When I eat a gherkin"
        And a step function for "When I eat a gherkin" exists
       When the step is tested
        And the step function reports failure
       Then the step should fail
        And the step's test status should be "failed"

    Scenario: passing step functions

      Given a step description "When I eat a gherkin"
        And a step function for "When I eat a gherkin" exists
       When the step is tested
        And the step function reports success
       Then the step should pass
        And the step's test status should be "ok"

    # TODO: 2021-11-04 move to a separate feature - validate entire feature suite
    Scenario: unspecified steps

      Given a step function for "When I eat a gherkin" exists
        But no step description for "When I eat a gherkin" exists
       When all tests are run
       Then the step function should be reported as "unspecified"
