# Features

* [Ensure that all features are tested](ensure_that_all_features_are_tested.feature)

  As a developer
  I want to ensure that no features are untested before committing any changes
  So that the statement "no errors" actually means "all features passed"

