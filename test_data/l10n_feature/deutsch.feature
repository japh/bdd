#language: de

Funktion: a german feature to test the gherkin parser

  It would be REALLY useful to know if each step is supposed to be a 'Given'
  (context), 'When' (action) or 'Then' (outcome) step.
  (with suitable mappings for 'And' and 'But')

  However, it seems that the AST produced by the official gherkin parser
  doesn't provide this information, so we're a bit blind.

  However, the AST does have the original, localised (and therefore useless)
  keyword which was used for each step.

  # *sigh* *face-palm*

  # By the way, the gherkin parser baulks if you put comments before or within
  # your description text....
  # or put another way, comments MUST appear AFTER any descriptions
  # *double-face-palm*

  Hintergrund: manchmal muss man einfach essen und trinken

    Angenommen ich habe Hunger
           Und ich habe durst

  Regel: esse etwas, wenn Du Hunger hast

    Essen ohne zu trinken ist keine gute Idee.
    Lebenserwartung: kurz

    Hintergrund: hunger stillt kein Durst
      Angenommen es gibt nichts zum trinken

    Beispiel: essen ohne trinken
      Angenommen ich habe <food>
            Wenn <food> heiss ist
            Dann esse ich <food>
            Aber ich bleibe dürstig
      Beispiele:
        | food  |
        | Pizza |
        | Kekse |

  Regel: trinke etwas, wenn Du Dürst hast

    Trinked ohne zu essen ist keine gute Idee...
    aber immerhin etwas besser als andersrum.
    Lebenserwartung: etwas länger

    Hintergrund: man kann eine Weile lang ohne essen leben
      Angenommen es gibt nichts zum essen

    Beispiel: trinken ohne essen
      Angenommen ich habe <drink>
            Wenn <drink> heiss ist
            Dann esse ich <drink>
            Aber ich bleibe dürstig
      Beispiele:
        | drink  |
        | Kaffee |
        | Tee    |

  Regel: nimm alles wenn Du es kannst

    Nur wenn man isst und trinkt bleibt man längerfristig gut drauf.
    Lebenserwartung: so lange wie Du willst

    Hintergrund: es gibt alles
      Angenommen es gibt genug zum essen
                 | food  |
                 | Pizza |
                 | Kekse |
             Und es gibt genug zum trinken
                 """
                 on the house
                 """

    Beispiel: essen und trinken
      Angenommen ich habe <food>
             Und ich habe <drink>
            Wenn <food> heiss ist
             Und <drink> heiss ist
            Dann trinke ich <food>
             Und ich esse <drink>
                 # you have to love german Grammar!
                 # "dann trinke ich ... und ich esse ..."
      Beispiele:
        | food  | drink  |
        | Pizza | Kaffee |
        | Kekse | Tee    |
