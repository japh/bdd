
# this comment should not cause any problems

Feature: comments *should* be allowed on *any* line in a gherkin document

  # this comment causes the v22 gherkin compiler to crash

  Comments should be allowed anywhere, before the first keyword, after the last
  step, theoretically, within a data or examples table.

  Comments within docstrings need to be handled by the step definition!
