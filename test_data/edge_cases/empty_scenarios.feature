Feature: empty scenarios

  The discussion in https://github.com/cucumber/common/issues/249
  addresses the fact that scenarios without steps should cause a warning, but
  also not perform any tests.

  Open questions:
    - should background steps be run?
    - should outlines generate multiple (empty) scenario pickles?
    - should the author be forced to use a tag to prevent incomplete scenarios
      from being pickled?
      drawbacks:
      - no hint that the BDD process is incomplete
      - bad UX, pushing the onus on the user to "get it right" instead of
        nudging them in the right direction

  Background:
    Given something

  Scenario: empty scenario

  Scenario: another empty scenario

  Rule: empty scenarios should be reported as "undefined"

    Given something else

    Scenario: undefined

    Scenario: undefined outline
      Examples:
        | number |
        | one    |
        | two    |
