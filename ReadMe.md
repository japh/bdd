# opensauce.de/bdd

Yet another Behavior-Driven Development (BDD) / Cucumber / Gherkin Tool for Golang

## Why?

As of 2021 there are already 2 well known and more-or-less official BDD /
Cucumber / Gherkin testing implementations for go, namely:

- [godog](https://github.com/cucumber/godog)
- [gobdd](https://github.com/go-bdd/gobdd)

However, sadly, my experience with both has been dismal.

This is 'yet another attempt'™ to get it right, heavily influenced by the
existing go implementations.

### Problems with `godog`

- as of 2021-10-24 `godog` simply panics whenever I try to implement the example
  on `godog`'s introduction page.
- doesn't integrate nicely with `go test`
- doesn't support the `Rule:` keyword
- doesn't differentiate between Given/When/Then steps

### Problems with `gobdd`

- minimal documentation
- doesn't support the `Rule:` keyword
  - I managed to hack an update to a newer version of go-gherkin and
    go-messages, which works, a bit better, and understands the 'Rule:' keyword,
    but still doesn't handle errors well
- doesn't differentiate between Given/When/Then steps
- very easy to get *success by running no tests*
- no sanity checks for missing / extraneous step definitions

### Underlying problems with the official cucumber parser

- the official parser uses a context free grammer, defined in a variant of Bachus Naur form ((E)BNF)
  - however:
    - the addition of `tags` *before* structural keywords such as `Feature:`
      or `Scenario:` turned Gherkin into a **Non**-Deterministic Context Free Language

## Goals

- [ ] help developers ensure that their BDD / feature specifications are fully
	  tested (feature coverage, as apposed to code coverage)
- [ ] provide additional tools for validating features
- [ ] provide the ability to generate feature indexes and overviews
- [ ] differentiate between step types for step definitions
	- the generic `*` step is *not supported*
	- if it would be supported, it would probably be like `And`, but default to `Then` (i.e. perform a check)
- [ ] run full or partial tests

## Technical Goals

- [ ] use official cucumber libraries as much as possible
  - [ ] [gherkin-go](https://github.com/cucumber/gherkin-go)
  - [ ] [messages-go](https://github.com/cucumber/messages-go)
- [ ] explore the possibilities offered by side-stepping the official libraries

