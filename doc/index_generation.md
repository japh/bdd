# Index Generation

The Gherkin Abstract Syntax Tree (AST) can also be used to generate a *Project
Index*. e.g. `features/ReadMe.md` 

The index contains an overview of all features and their descriptions to enable
better orientation for people who are new to the project.

Clicking on a feature opens the actual feature file.

In order to improve BDD's *story telling* nature, the features may be manually
ordered, allowing the ordering and grouping of features by their relevance,
rather than their names.

Just like any good book, features should tell a story, with an introduction,
beginning, middle and end (details).
