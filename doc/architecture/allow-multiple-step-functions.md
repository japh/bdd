# Allow Multiple Step Functions

**Status:** accepted (2021-11-13)

## Context

When testing gherkin features, it can occur that:
- a step has no matching step function
  - an error, the step is untestable
- a step matches exactly one step function
  - normal
- a step matches multiple step functions
  - allowed if the duplicates are unambiguous
    - e.g.: "Given I am awake" and "When I am awake"
  - allowed if different functions match for different examples e.g.:
    - the step "When I eat <number> gherkins"
    - may match "When I eat 2 gherkins" (pattern: <when> "I eat {int} gherkins")
    - and "When I eat too many gherkins" (pattern: <when> "I eat {text} gherkins")
  - an error otherwise
- a step function exists which does not match any steps
  - OK if a sub-set of features is being processed
    - but only if tag filtering occurs prior the step => function matching phase
  - an error otherwise

## Decision

* always allow for multiple functions to match any individual step
  * provide warnings / errors if step functions are ambiguous
  * if a step matches 'some times', list the matching functions when reporting
    the "no function found" error

## Consequences

- improved error reporting
