# Architecture Decisions

The files in this directory document architectural decisions along with their
context and consequences.

In general, each file should contain:

* **Title**: short present tense imperative phrase, less than 50 characters, like a git commit message.

* **Status**: proposed, accepted, rejected, deprecated, superseded, etc. including a date stamp.

* **Context**: what is the issue that we're seeing that is motivating this decision or change.

* **Decision**: what is the change that we're actually proposing or doing.

* **Consequences**: what becomes easier or more difficult to do because of this change.
