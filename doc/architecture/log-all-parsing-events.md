# Log All Parsing Events

**Status:** accepted (2021-11-12)

## Context

The processing of a suite of Gherkin documents can, and should, be split into multiple, independent *phases*, e.g.:
- scanning (see the 'Line Oriented Parsing' decision)
- parsing
- compiling
- test matching
- test execution
- reporting

## Decision

- each phase of processing should emit a stream of *events*, e.g.:
    - scanner produces `line` events
    - the parser produces `keyword`, `warning` and `error` events, referring to the lines directly preceding them
    - the compiler produces `test suite` and `test step` events for all scenarios and individual outlines
    - the test runner produces `test run` events
- all events may be passed between phases either in-memory, via objects, or
  accross process boundaries via a serialisation format such as NDJSON or
  similar.
- events may be evaluated and replayed independently of an actual test run
- events may be re-formatted to any format for further consumption, for example:
  - CLI output (STDOUT / STDERR)
  - log file
  - logging server
  - spreadsheet
  - HTTP REST API
  - ...
- event streams may be prepared for human consumption or further fully automated processes
- events may be filtered at any stage of processing
  - however, if possible, no data should be discarded during processing so as
    not to preclude any further processes downstream

## Consequences

* by producing a stream of events, we enable...
  - the use of various stages of processing for very different tasks, such as:
    - automatic formatting of gherkin documents
    - static feature analysis (how many test functions match each step etc)
    - testing
    - reporting
    - statistical (trend) analysis
