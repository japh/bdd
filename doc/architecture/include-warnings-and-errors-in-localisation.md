# Include warnings and errors in localtions

**Status:** accepted (2021-11-13)

## Context

Currently all errors produced by the gherkin parser are in english.

## Decision

* errors should match the language of each gherkin document
  * allow users to determine a language for all parser messages

* extend the languages.json localisation file to include
  * keywords
  * warning messages with placeholders
  * error messages with placeholders

## Consequences

- People who do not understand english can still understand their gherkin errors

