# Fuzzy-Match Keywords

**Status:** accepted (2021-11-13)

## Context

Some errors can be difficult to spot.

e.g.:

- spurious spaces:
  ```
  # can you see what is wrong here?
  Feature: test
    Scenario : what is wrong here?
      Then a warning should draw attention to the additional space between 'Scenario' and ':'
      And the keyword should be accepted
  ```

- missing spaces:
  ```
  Businessneed: test
  ```

- incorrect capitalisation
  ```
  # a feature entirely in lowercase still make sense to humans
  # warn the user but do not abort processing
  feature: test
    scenario: don't be picky
      when keywords appear with incorrect capitalisation
      then a warning should indicate the correct keyword capitalisation
      and the keyword should be accepted
  ```

- steps outside a scenario
  ```
  Feature: test
    # the following is a feature description, but it looks like the author
    # simply forgot the Scenario: line
    Given a description in which each line begins with an otherwise valid step keyword
    Then the steps should be treated as a description
    And a warning should indicate that the description looks suspiciously like a scenario
  ```

- unnamed features and rules
  - features and rules *must always* have a name
  - backgrounds, scenarios and outlines may be anonymous

## Decision

* be lenient when detecting keywords
  * allow for syntactic variations
    e.g. lowercase instead of uppercase, additional or missing spaces etc.
  * always report lenient matches as warnings
    * the events produced should be suitable for re-formatting the gherkin
      document using the preferred keywords and formatting

* look for keywords in descriptions
  * if the trailing lines of a description all begin with step keywords
    (Given, When, Then, ...) then warn that the user may have fogotten the
    preceding 'Scenario:' line
    

## Consequences

- improved error reporting
