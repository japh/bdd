# Strategic Design

The design of this BDD tool is intended to facilitate the ease of use for at
least two very distinct groups of stakeholders:

### `PO's` (product owners and similar roles)
  - people who define *what* the project should achieve
  - these people typically:
    - write gherkin features
    - wish to see reports showing the current progress of feature
      development and validating existing features.
  - just want a set of simple, understandable rules to follow
    - the parser / compiler must be as lenient as possible
    - cryptic problems should be avoided

### `dev's` (developers, testers etc.)
  - people who need to read gherkin files
  - write functions to test individual features
  - run tests to ensure that features are working as expected

Since this system is being used by such a varied collection of people, a
flexible and easily accessable user experience is of utmost importance.

For example:
    - compiler crashes are not acceptable
      - the parser / compiler must report errors in a human friendly manner
      - all error messages should not only indicate what went wrong, but also suggest a possible solution
      - error messages must be succinct and indicate exactly where the error occurred (file, line, column)
    - PO's should not be expected to work with development tools such as git, command line etc.
    - PO's should be able to validate their gherkin files without reference to any project code
    - PO's should be able to validate that the results presented to them
      (e.g. via a test report) actually relate to the feature that they described.
      - it should be possible to re-construct the gherkin features which were tested from the report.
    - dev's should not perceive the BDD tooling as a hindrance:
      - run-times should be fast
      - testing and reporting should be easily automated
        - errors and results should be loggable and easily evaluated at a later time
        - command line output should be configurable
        - the set of tests to run should be easily reduced to that which is of interest to the developer
        - incomplete tests should be easily identifiable (e.g.: "succeeded, but skipped ... tests")

## Strategic Goals

The strategies taken by this project are:

### multi-representation:
  - all processing results are emitted in a common manner (stream of
    events) such that the events can be used for:
      - gherkin syntax highlighting (for PO's and dev's while editing)
      - gherkin parser warnings and errors (for PO's and dev's while editing)
      - expansion of outlines (for PO's and dev's while editing)
      - mapping to step functions (for dev's while developing, debugging, testing)
      - test execution (automated or for dev's while developing, debugging, testing)
      - test reporting:
        - detailed reports for developers / testers
        - summarised reports (various levels of detail) for PO's and dev's
      - test reconstruction
        - the ability to reconstruct what *was* tested when the only data
          available is the event stream.

### line based:
  - the gherkin language is "line based"
  - all parser and compiler processing retains file and line references

### reconstructability:
  - all phases of parsing, compiling and testing are emitted as events
  - events are emitted on a line-by-line basis
  - events may refer to previous events, thus making reconstruction possible
  - parser and compiler events are emitted 'in order'
  - testing events may be emitted concurrently:
    - tests may be run in parallel
      - e.g.: 10 tests may start at the same time, and their results are
          emitted as they become available
    - the start & finish events for an individual test appear sequentially
      - but any number of other events may appear between them

### multi-phased processing:

By splitting the processing into multiple phases, different users can

#### Phase 1: Session
  - each session produces a single stream of events
  - each session processes any or all available gherkin documents
    - any selection of individual features may be processed
    - by default, all available features are processed

#### Phase 2: Parsing
  - parses a single gherkin document
  - produces at least 1 event per parsed line
    - adds 'warning' or 'error' events for lines which don't match the gherkin language
    - for example, if tags appear before a background, a warning is produced that the background may not be tagged.
      - the tag objects remain in the stream
      - the background object remains in the stream
      - however, the tags are NOT associated with the background
        and will NOT be retained for any following scenarios or rules.
      - e.g.:

          ```
          parse begin <id>
          document <uri>
          language <code>
          tag <name>
          comment <text>
          other <text>
          tag <name>
          feature <name>
          empty
          comment <text>
          empty
          other <text>
          tag <name>
          background <name>
          warning "background cannot be tagged"
          wanning "tag <name> from <line> <column> ignored"
          given <name>
          given <name>
          empty
          scenario <name>
          given <name>
          when <name>
          ditto <name>
          then <name>
          ditto <name>
          parse end <id>
          ```

#### Phase 3: Filtering
  - tag filters are applied
  - gherkin objects which do not match the tag filter are omitted from further processing
  - e.g.:
      ```
      skipped <feature id>
      skipped <rule id>
      skipped <scenario id>
      ...
      ```

#### Phase 4: Compiling (pickling)
  - reduces gherkin documents to a series of scenarios and steps
  - scenarios refer to their respective rules or features
  - scenarios are repeated for each of their outlines
  - each scenario instance references the outline row being tested
  - the steps for each scenario consist of:
    - any feature-background steps
    - any rule-background steps
    - all of the scenarios steps
  - any <...> placeholders in the step names are replaced by their
    appropriate values from the current outline table row.
  - e.g.:
      ```
      pickled scenario <scenario id> <outline row id>
      pickled step <step id> <outline cell ids> <pickled name>
      pickled step <step id> <outline cell ids> <pickled name>
      pickled step <step id> <outline cell ids> <pickled name>
      pickled step <step id> <outline cell ids> <pickled name>
      pickled scenario <scenario id> <outline row id>
      pickled step <step id> <outline cell ids> <pickled name>
      pickled step <step id> <outline cell ids> <pickled name>
      pickled step <step id> <outline cell ids> <pickled name>
      pickled step <step id> <outline cell ids> <pickled name>
      ```

#### Phase 5: Test Matching
  - match each pickled name (with expanded outline values) against known step function patterns
    - a step name may match different functon patterns depending on the outline values

e.g.: a scenario like this:
    ```
    Scenario: handle different value types
      When I get <value> of <type>
    Outline:
      | type | value |
      | int  | 1     |
      | word | hello |
    ```

Could match the step functions:

  -     `When 'I get {int} of {word}'  => test_when_i_have_an_int()`
  - and `When 'I get {word} of {word}' => test_when_i_have_a_word()`

If a step has matched one or more functions but doesn't match a specific
outline, this should be reported also.

e.g.:
    ```
    error no test function found for step <id> When "I get <value> of <type>" from <location>
    outline reference <location> "| string | multiple words |"
    outline value: <type>  = string
    outline value: <value> = multiple words
    suggestion step <id> did match the function for When "I get {int} of {word}"
    suggestion step <id> did match the function for When "I get {word} of {word}"
    ```

If a step name doesn't match a function of the same
Given/When/Then type, but it does match a different type,
suggest that either the step or the function has been
incorrectly defined.

    ```
    error no test function found for step <id> Then "I get <value> of <type>" from <location>
    suggestion did you mean: When "I get {int} of {word}" ?
    suggestion did you mean: When "I get {word} of {word}" ?
    ```

#### Phase 6: Test Execution

  - events are emitted whenever a feature, rule, scenario, outline or step starts or finishes
  - e.g.:

      ```
      start  session      ---------+
      start  feature      -------+ |
      start  rule         -----+ | |
      start  outline row  ---+ | | |
      start  scenario     -+ | | | |
      start  step          | | | | |
      finish step          | | | | |
      start  step          | | | | |
      finish step          | | | | |
      finish scenario     -+ | | | |
      finish outline      ---+ | | |
      start  outline row  ---+ | | |
      start  scenario     -+ | | | |
      start  step          | | | | |
      finish step          | | | | |
      start  step          | | | | |
      finish step          | | | | |
      finish scenario     -+ | | | |
      finish outline      ---+ | | |
      finish rule         -----+ | |
      finish feature      -------+ |
      finish session      ---------+
      ```

  - the user doesn't need to know that the events exist
    - e.g.:
      - events may be stored to a file or streamed to a database such as elestic search etc.
      - the command line may filter out everything except errors and a simple final summary
        - more details may be produced depending on command line options
      - a web UI may use the stream of events to show an interactive
        representation of the set of features and their respective status

#### Phase 8: Reporting
  - reporting may be either integrated or provided separately

    - integrated reporting processes the events internally
      - e.g. CLI produces output on STDOUT and STDERR
      - integrated reporting does not preclude storage of the event stream for later analysis.
    - separate reporting processes a stream of events from a previously stored
      stream, or directly as they are emitted.
      - any number of separate reporting forms may be available, e.g.:
        - status summary
        - fully detailed analysis
        - statistical analysis
        - feature re-construction with syntax highlighting (living documentation)

## Technical Details:

  - All events must be available in a common format, e.g.: `NDJSON`
  - All event streams must always begin with a version number
  - All event streams must begin with a timestamp
  - All event streams should include a reference to the project version being processed
  - All trailing phases after the parsing phase may be omitted
    - e.g.:
      - session, parsing
      - session, parsing, filtering
      - session, parsing, filtering, compiling
      - session, parsing, filtering, compiling, function matching
      - session, parsing, filtering, compiling, function matching, testing
  - The reporting phase is always the last phase, and may be used separately
    based on a previously store stream of events.
