#!/bin/sh

#
# update and include the features index as part of the commit process,
# but only if the index is missing or *.feature files have changed.
#

#
# Git Workspace Installation:
#
#   cd <your project's git workspace>
#   ln -s ../../util/pre-commit-git-hook.sh .git/hooks/pre-commit
#

# all paths relative to workspace root
feature_dir="features/"
feature_index="${feature_dir}ReadMe.md"
gfindex="util/gfindex"

# ---------------------
# No changes below here

if [[ -z $gfindex ]] || [[ ! -x $gfindex ]]
then
    echo "Please configure the path to gfindex"
    exit 1
fi

if [[ ! -d $feature_dir ]]
then
    echo "Missing feature dir: ${feature_dir}"
    exit 1
fi

index_exists=$( test -f "${feature_index}" )
changed_features=$( git status --porcelain "${feature_dir}**.feature" | wc -l | tr -d ' ' )

if [[ $index_exists != 0 ]]
then
    echo "Creating $feature_index"
    generate=1
elif [[ $changed_features != 0 ]]
then
    echo "Updating $feature_index"
    generate=1
fi

if [[ $generate != 0 ]]
then
    "${gfindex}" --index "${feature_index}"
    git add "${feature_index}"
fi

exit
