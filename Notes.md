# Technical Notes For Developers

## Use of cucumber's official libraries

This project uses cucumber's gherkin-go and messages-go libraries to parse a
project's feature files.

- `gherkin.ParseGherkinDocument()` parse a single `.features` file and
    return an AST of message objects (starting with a GherkinDocument)

- `gherkin.Pickles()` convert a `GherkinDocument` object into a stream of
    executable pickle objects

## Context objects

Instead of using a complex context mechanism, just pass a user-definable struct
(\*interface{}) to each step.

## Named steps

Reduce ambiguity by requiring steps to be defined with their Given/When/Then context.

- see also [cucumber issue #768](https://github.com/cucumber/common/issues/768)
  - may become `context` (`Given`), `action` (`When`) and `outcome` (`Then`)
