package bdd

import (
	"fmt"
	"reflect"
	"testing"

	"gopkg.in/yaml.v3"
	want "opensauce.de/bdd/internal/expectation"
)

// FIX: 2021-10-28 t.Helper() is supposed to place a marker on the stack
//                 indicating which file/line should be reported when t.Log() &
//                 co. are called by functions further down in the stack
//                 i.e. TestA() => assert(x==y) => t.Error("oops")
//                 Why does it do nothing?

func TestSuiteCreation(t *testing.T) {
	t.Helper()
	s := NewSuite(t)
	if !want.NotNil(t, s, "create a BDD Test Suite") {
		return
	}

	//
	// By default, an unconfigured test suite should find ALL gherkin documents
	// in the features directory
	//
	// Since this project HAS gherkin documents, this should always be > 0
	//
	// if len(r.gherkinDocumentNames()) == 0 {
	// 	t.fail("Found no gherkin documents")
	// }
}

func TestDefaultSuiteOptions(t *testing.T) {
	t.Helper()
	s := NewSuite(t)
	if !want.NotNil(t, s, "create a BDD Test Suite") {
		return
	}

	o := s.Options()
	if !want.NotNil(t, o, "get suite options for test interrogation") {
		return
	}
	if !want.Equal(t, 1, len(o), "should be just 1 default option") {
		return
	}

	want.Equal(t, "Search Path: features/", o[0], "expected default option")
}

func TestSuiteOptions(t *testing.T) {
	t.Helper()
	f := WithFeaturesPaths("features/f1.feature", "features/f1.details/")
	if !want.NotNil(t, f) {
		return
	}
	want.Equal(t, reflect.Func, reflect.ValueOf(f).Kind(), "function options should always return a function which can be applied to an object")

	// Note: we get a panic if f is nil = even though we guard against it above :-|
	s := NewSuite(t, f)
	if !want.NotNil(t, s, "create a BDD Test Suite") {
		return
	}

	// change an object's options after creation
	f(s)

	o := s.Options()
	if !want.NotNil(t, o, "options should be not nil") {
		return
	}
	if !want.Equal(t, len(o), 2, "2 options") {
		return
	}

	want.Equal(t, o[0], "Search Path: features/f1.feature", "features/f1.feature")
	want.Equal(t, o[1], "Search Path: features/f1.details/", "features/details/")
}

type dirScanUnit struct {
	name       string
	dir        string
	haveErrors bool
	fileCount  int
}

func TestGherkinFileFinder(t *testing.T) {
	t.Helper()
	units := []*dirScanUnit{
		{
			name:       "missing gherkin dir",
			dir:        "test_data/missing_features",
			haveErrors: true,
		},
		{
			name:       "empty gherkin dir",
			dir:        "test_data/no_features",
			haveErrors: true,
			fileCount:  0,
		},
		{
			name:      "non-empty gherkin dir",
			dir:       "test_data/some_features",
			fileCount: 2,
		},
		{
			name:      "deep gherkin dir",
			dir:       "test_data/deep_features",
			fileCount: 3,
		},
	}

	for _, u := range units {
		t.Run(u.name, func(t *testing.T) { testFileFinderUnit(t, u) })
	}
}

func testFileFinderUnit(t *testing.T, u *dirScanUnit) {
	s := NewSuite(t, WithFeaturesPaths(u.dir))
	if !want.NotNil(t, s, "create a BDD Test Suite") {
		return
	}

	t.Logf("scanning dir: %s", u.dir)
	s.findGherkinDocuments()

	e := s.Errors()
	haveErrors := len(e) != 0
	f := s.gherkinDocuments
	fileCount := 0
	if f != nil {
		fileCount = len(f)
	}

	want.Equal(t, u.haveErrors, haveErrors, fmt.Sprintf("got errors from directory '%s'", u.dir))
	for _, m := range e {
		t.Logf("=> %s", m)
	}
	want.Equal(t, u.fileCount, fileCount, fmt.Sprintf("file-count from directory '%s'", u.dir))
}

func TestParseGherkinFiles(t *testing.T) {
	t.Helper()
	s := NewSuite(t, WithFeaturesPaths("test_data/l10n_feature"))
	if !want.NotNil(t, s, "create a BDD Test Suite") {
		return
	}

	t.Log("test suite documents", s.gherkinDocumentNames())

	// shortName := "ensure_that_all_features_are_tested"
	shortName := "deutsch"
	gherkinPath, err := s.gherkinDocumentPath(shortName)
	want.Nil(t, err, fmt.Sprintf("should find '%s'", shortName))
	want.NotNil(t, gherkinPath, fmt.Sprintf("path to '%s'", shortName))

	gherkinAst, err := s.gherkinAST(gherkinPath)
	want.Nil(t, err, fmt.Sprintf("should be able to parse '%s'", shortName))
	want.NotNil(t, gherkinAst, fmt.Sprintf("AST for '%s' should not be nil", shortName))

	y, err := yaml.Marshal(gherkinAst)
	want.Nil(t, err, fmt.Sprintf("should be able to marshal AST for '%s'", shortName))
	t.Logf("actually got an AST, apparently\n%s\n", y)
}

func TestPickles(t *testing.T) {
	t.Helper()
	s := NewSuite(t, WithFeaturesPaths("test_data/l10n_feature"))
	if !want.NotNil(t, s, "create a BDD Test Suite") {
		return
	}

	t.Log("test suite documents", s.gherkinDocumentNames())

	// shortName := "ensure_that_all_features_are_tested"
	shortName := "deutsch"
	gherkinPath, err := s.gherkinDocumentPath(shortName)
	want.Nil(t, err, fmt.Sprintf("should find '%s'", shortName))

	pickles, err := s.GherkinPickles(gherkinPath)
	want.Nil(t, err, fmt.Sprintf("should be able to get pickles for '%s'", shortName))
	want.NotNil(t, pickles, fmt.Sprintf("Pickles for '%s'", shortName))

	y, err := yaml.Marshal(pickles)
	want.Nil(t, err, fmt.Sprintf("should be able to marshal Pickles for '%s'", shortName))
	t.Logf("actually got some pickles apparently\n%s\n", y)
}
