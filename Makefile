.PHONY:	all doc test build lightclean clean distclean 
.PHONY:	features rules scenarios steps

FEATURE_FILES := $(shell find features -name \*.feature -print)
FEATURE_INDEX := features/ReadMe.md

# Build documentation first,
# then run tests,
# then build something that can be deployed if the tests are successful

all:	doc build test

# .SILENT must NOT be the first (default) rule
.SILENT:

doc:	$(FEATURE_INDEX)

$(FEATURE_INDEX): $(FEATURE_FILES)
	echo "Building $(FEATURE_INDEX)"
	./util/gfindex --index $(FEATURE_INDEX)

test:
	echo "Unit tests"
	go fmt  ./...
	go vet  ./...
	golint  ./...
	go test ./...

build:
	echo "Building all dependencies"
	go build -v ./...

lightclean:
	echo "Cleaning golang test cache"
	go clean -testcache

clean: lightclean
	echo "Files not in git:"
	git clean -dxn

distclean: lightclean
	echo "Deleting files not in git"
	git clean -dxf

# BDD / Gherkin support
# 	Each of the following rules provide an overview of the defined BDD
# 	features, in varying levels of detail.
# 	TODO: 2021-10-23 replace grep with something more intelligent, 
# 	ie: something that actually parses gherkin and can provide a structure
# 	    for reporting test success/failure status
features:
	grep -R -h '^\(Ability\|Feature\):' features/

feature-rules:
	grep -R -h '^\(Ability\|Feature\|\s\+Rule\):' features/

# should include examples: tables (1 test per table row)
feature-scenarios:
	grep -R -h '^\(\(\(Ability\|Feature\|Business Need\)\|\s\+\(Rule\|Scenario\|Example\|Scenario Outline\)\):\)' features/

feature-steps:
	grep -R -h '^\(\(\(Ability\|Feature\)\|\s\+\(Rule\|Background\|Scenario\):\)\|\s*\(Given\|When\|Then\|And\|But\|\*\)\s\+\)' features/

unique-steps:
	grep -R -h '^\s\+\(Given\|When\|Then\|And\|But\|\*\)\s\+' features/ \
		| perl -pe 's/^\s+|\s+(\v)/$$1/g' \
		| perl -pe 's/(^|\s)(?:"[^"]*"|<[^>]*>|[+-]?\d*(?:\.\d+)?)(\s|\v)/$${1}{}$${2}/g' \
		| perl -pe 's{^(And|But|\*)\b}{($$current // $$1)}e; /^(\w+)/; $$current = $$1' \
		| sort -i -u
